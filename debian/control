Source: iolang
Section: devel
Priority: optional
Maintainer: Yangfl <mmyangfl@gmail.com>
Build-Depends:
 debhelper (>= 11),
 cmake,
 libaio-dev,
 libgmp-dev,
 libcairo2-dev,
 libclutter-1.0-dev, libatk1.0-dev, libgtk2.0-dev, libpango1.0-dev,
 libncurses5-dev,
 libdbi-dev,
 libedit-dev,
 mesa-common-dev, freeglut3-dev, libfreetype6-dev, libxmu-dev,
 libjpeg-dev,
 libpng-dev,
 libtiff-dev,
 libsndfile1-dev,
 libxml2-dev,
 libloudmouth1-dev,
 liblzo2-dev,
 libmariadbclient-dev,
 libogg-dev,
 python3-dev (>= 3.3),
 libqdbm-dev,
 libreadline-dev,
 libpcre3-dev,
 libsamplerate0-dev,
 libssl-dev,
 libevent-dev,
 libsqlite3-dev,
 libtheora-dev,
 libtokyocabinet-dev,
 uuid-dev,
 libvorbis-dev,
 libyajl-dev,
 zlib1g-dev,
# AsyncRequest: libaio-dev
# AVCodec: libavformat-dev, libavcodec-dev, libavutil-dev, libavdevice-dev  # XXX
# BigNum: libgmp-dev
# Cairo: libcairo2-dev
# CFFI: libffi-dev  # XXX
# Clutter: libclutter-1.0-dev, libatk1.0-dev, libgtk2.0-dev, libpango1.0-dev, libcairo2-dev
# Curses: libncurses5-dev
# DBI: libdbi-dev
# EditLine: libedit-dev
# Font: mesa-common-dev, freeglut3-dev, libfreetype6-dev, libxmu-dev
# GLFW: libglfw3-dev, libxmu-dev  # XXX
# Image: libjpeg-dev, libpng-dev, libtiff-dev
# LibSndFile: libsndfile1-dev
# Libxml2: libxml2-dev
# Loudmouth: libgtk2.0-dev, libloudmouth1-dev
# LZO: liblzo2-dev
# MySQL: libmariadbclient-dev
# ODE: libode-dev  # XXX
# Ogg: libogg-dev
# OpenGL: mesa-common-dev, freeglut3-dev
# Python: python3-dev (>= 3.3)
# QDBM: libqdbm-dev
# ReadLine: libreadline-dev
# Regex: libpcre3-dev
# SampleRateConverter: libsamplerate0-dev
# SecureSocket: libssl-dev
# Socket: libevent-dev
# SQLite3: libsqlite3-dev
# TagLib: libtag1-dev  # XXX
# Theora: libtheora-dev
# TokyoCabinet: libtokyocabinet-dev
# UUID: uuid-dev
# Vorbis: libvorbis-dev
# Yajl: libyajl-dev
# Zlib: zlib1g-dev
Standards-Version: 4.1.3
Homepage: http://iolanguage.org/
Vcs-Git: https://salsa.debian.org/yangfl-guest/io.git
Vcs-Browser: https://salsa.debian.org/yangfl-guest/io

Package: iolang
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: iolang-addons
Suggests: iolang-doc
Description: Dynamic prototype-based programming language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).

Package: iolang-addons
Architecture: any
Depends:
 iolang-asyncrequest,
 iolang-bignum,
 iolang-bitly,
 iolang-blowfish,
 iolang-box,
 iolang-cairo,
 iolang-cgi,
 iolang-clutter,
 iolang-continuedfraction,
 iolang-curses,
 iolang-dbi,
 iolang-distributedobjects,
 iolang-editline,
 iolang-facebook,
 iolang-flux,
 iolang-fnmatch,
 iolang-font,
 iolang-googlesearch,
 iolang-httpclient,
 iolang-image,
 iolang-libsndfile,
 iolang-libxml2,
 iolang-loki,
 iolang-loudmouth,
 iolang-lzo,
 iolang-md5,
 iolang-mysql,
 iolang-notificationcenter,
 iolang-obsidian,
 iolang-ogg,
 iolang-opengl,
 iolang-postgresql,
 iolang-python,
 iolang-qdbm,
 iolang-random,
 iolang-range,
 iolang-rational,
 iolang-readline,
 iolang-regex,
 iolang-securesocket,
 iolang-sgml,
 iolang-sha1,
 iolang-socket,
 iolang-sqldatabase,
 iolang-sqlite3,
 iolang-syslog,
 iolang-systemcall,
 iolang-theora,
 iolang-thread,
 iolang-tokyocabinet,
 iolang-twitter,
 iolang-user,
 iolang-uuid,
 iolang-vertexdb,
 iolang-volcano,
 iolang-vorbis,
 iolang-yajl,
 iolang-zlib,
 ${misc:Depends},
Description: addons for io language (metapackage)
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package is a metapackage which depends on all io addons.

Package: iolang-asyncrequest
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: AsyncRequest addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the AsyncRequest addon.

Package: iolang-bignum
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: BigNum addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the BigNum addon.

Package: iolang-bitly
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Bitly addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Bitly addon.

Package: iolang-blowfish
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Blowfish addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Blowfish addon.

Package: iolang-box
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Box addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Box addon.

Package: iolang-cairo
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Cairo addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Cairo addon.

Package: iolang-cgi
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: CGI addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the CGI addon.

Package: iolang-clutter
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Clutter addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Clutter addon.

Package: iolang-continuedfraction
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: ContinuedFraction addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the ContinuedFraction addon.

Package: iolang-curses
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Curses addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Curses addon.

Package: iolang-dbi
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: DBI addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the DBI addon.

Package: iolang-distributedobjects
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: DistributedObjects addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the DistributedObjects addon.

Package: iolang-editline
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: EditLine addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the EditLine addon.

Package: iolang-facebook
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Facebook addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Facebook addon.

Package: iolang-flux
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: fonts-freefont-ttf, ttf-bitstream-vera
Description: Flux addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Flux addon.

Package: iolang-fnmatch
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Fnmatch addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Fnmatch addon.

Package: iolang-font
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Font addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Font addon.

Package: iolang-googlesearch
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: GoogleSearch addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the GoogleSearch addon.

Package: iolang-httpclient
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: HttpClient addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the HttpClient addon.

Package: iolang-image
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Image addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Image addon.

Package: iolang-libsndfile
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: LibSndFile addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the LibSndFile addon.

Package: iolang-libxml2
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Libxml2 addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Libxml2 addon.

Package: iolang-loki
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Loki addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Loki addon.

Package: iolang-loudmouth
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Loudmouth addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Loudmouth addon.

Package: iolang-lzo
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: LZO addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the LZO addon.

Package: iolang-md5
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: MD5 addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the MD5 addon.

Package: iolang-mysql
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: MySQL addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the MySQL addon.

Package: iolang-notificationcenter
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: NotificationCenter addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the NotificationCenter addon.

Package: iolang-obsidian
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Obsidian addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Obsidian addon.

Package: iolang-ogg
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Ogg addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Ogg addon.

Package: iolang-opengl
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: OpenGL addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the OpenGL addon.

Package: iolang-postgresql
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: PostgreSQL addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the PostgreSQL addon.

Package: iolang-python
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Python addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Python addon.

Package: iolang-qdbm
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: QDBM addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the QDBM addon.

Package: iolang-random
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Random addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Random addon.

Package: iolang-range
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Range addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Range addon.

Package: iolang-rational
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Rational addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Rational addon.

Package: iolang-readline
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: ReadLine addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the ReadLine addon.

Package: iolang-regex
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Regex addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Regex addon.

Package: iolang-securesocket
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: SecureSocket addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the SecureSocket addon.

Package: iolang-sgml
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: SGML addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the SGML addon.

Package: iolang-sha1
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: SHA1 addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the SHA1 addon.

Package: iolang-socket
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Socket addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Socket addon.

Package: iolang-sqldatabase
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: SqlDatabase addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the SqlDatabase addon.

Package: iolang-sqlite3
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: SQLite3 addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the SQLite3 addon.

Package: iolang-syslog
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Syslog addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Syslog addon.

Package: iolang-systemcall
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: SystemCall addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the SystemCall addon.

Package: iolang-theora
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Theora addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Theora addon.

Package: iolang-thread
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Thread addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Thread addon.

Package: iolang-tokyocabinet
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: TokyoCabinet addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the TokyoCabinet addon.

Package: iolang-twitter
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Twitter addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Twitter addon.

Package: iolang-user
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: User addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the User addon.

Package: iolang-uuid
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: UUID addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the UUID addon.

Package: iolang-vertexdb
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: VertexDB addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the VertexDB addon.

Package: iolang-volcano
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Volcano addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Volcano addon.

Package: iolang-vorbis
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Vorbis addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Vorbis addon.

Package: iolang-yajl
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Yajl addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Yajl addon.

Package: iolang-zlib
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: Zlib addon for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the Zlib addon.

Package: iolang-doc
Architecture: all
Depends: libjs-jquery, ${misc:Depends}
Section: doc
Multi-Arch: foreign
Description: documentation for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 This package contains the documentation in HTML format.

Package: libiobasekit-dev
Section: libdevel
Architecture: any
Depends: libiobasekit1 (= ${binary:Version}), ${misc:Depends}
Description: basekit for io language (development files)
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 The library contains the basekit for io language.
 .
 This package contains the header files.

Package: libiobasekit1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: basekit for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 The library contains the basekit for io language.

Package: libiocoroutine-dev
Section: libdevel
Architecture: any
Depends: libiocoroutine1 (= ${binary:Version}), ${misc:Depends}
Description: coroutine library for io language (development files)
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 The library contains the coroutine library for io language.
 .
 This package contains the header files.

Package: libiocoroutine1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: coroutine library for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 The library contains the coroutine library for io language.

Package: libiogarbagecollector-dev
Section: libdevel
Architecture: any
Depends: libiogarbagecollector1 (= ${binary:Version}), ${misc:Depends}
Description: garbage collector for io language (development files)
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 The library contains the garbage collector library for io language.
 .
 This package contains the header files.

Package: libiogarbagecollector1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: garbage collector for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 The library contains the garbage collector library for io language.

Package: libiovm-dev
Section: libdevel
Architecture: any
Depends: libiovm1 (= ${binary:Version}), ${misc:Depends}
Description: VM for io language (development files)
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 The library contains the VM library for io language.
 .
 This package contains the header files.

Package: libiovm1
Section: libs
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: VM for io language
 The ideas in Io are mostly inspired by Smalltalk (all values are objects),
 Self (prototype-based), NewtonScript (differential inheritance), Act1 (actors
 and futures for concurrency), LISP (code is a runtime inspectable/modifiable
 tree) and Lua (small, embeddable).
 .
 The library contains the VM library for io language.
